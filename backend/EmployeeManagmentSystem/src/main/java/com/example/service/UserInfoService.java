package com.example.service;

import java.util.List;
import java.util.Optional;

import com.exaple.entity.UserInfo;



public interface UserInfoService {

	List<UserInfo> getAllUserInfo();
	Optional<UserInfo> getSingleLeaveApplication(String id);
	String insertUserInfo(UserInfo info);
	String deleteUserInfo(String id);
}
