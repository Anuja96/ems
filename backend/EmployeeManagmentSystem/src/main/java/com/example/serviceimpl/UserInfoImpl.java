package com.example.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.reposetory.UserInfoRepo;
import com.example.service.UserInfoService;
import com.exaple.entity.UserInfo;



@Service
public class UserInfoImpl implements UserInfoService {

	@Autowired
	UserInfoRepo repo;
	
	 @PersistenceContext
	  private EntityManager em;
	@Override
	public List<UserInfo> getAllUserInfo() {
		List<UserInfo> info=(List<UserInfo>) repo.findAll();
		return info;
	}

	@Override
	public Optional<UserInfo> getSingleLeaveApplication(String id) {
		Optional<UserInfo> info=repo.findById(id);
		return info;
	}

	@Override
	public String insertUserInfo(UserInfo info) {
		repo.save(info);
		return "userinfoadded";
	}

	@Override
	public String deleteUserInfo(String id) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	@Transactional
//	public String deleteUserInfo(String id) {
//		// TODO Auto-generated method stub
//		
//		//em.createQuery("from UserInfo  where  userName='"+userName+"' and password='"+pass+"'", UserInfo.class).getResultList();
//		em.createQuery("delete UserInfo where userId="+Integer.valueOf(id));
//		return "user deleted sucessfully";
//	}

}
