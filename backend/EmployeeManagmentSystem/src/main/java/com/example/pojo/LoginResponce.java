package com.example.pojo;

public class LoginResponce {
	Boolean isSucess;
	String msg;
	String userType;
	int userId;
	public LoginResponce() {
		super();
	}
	
	
	public LoginResponce(Boolean isSucess, String msg, int userId) {
		super();
		this.isSucess = isSucess;
		this.msg = msg;
		this.userId = userId;
	}


	public Boolean getIsSucess() {
		return isSucess;
	}
	public void setIsSucess(Boolean isSucess) {
		this.isSucess = isSucess;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getUserType() {
		return userType;
	}


	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	
	

}
