package com.example.reposetory;

import org.springframework.data.repository.CrudRepository;

import com.exaple.entity.UserInfo;


public interface UserInfoRepo extends CrudRepository<UserInfo, String> {

}
