package com.example.mycontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.pojo.LoginResponce;

import com.example.service.UserInfoService;

import com.exaple.entity.UserInfo;

@RestController
public class MyController {
	@Autowired
	UserInfoService userInfo;
	

	
	@GetMapping("/hello")
	public String getHello() {
		//return"Hiiii";
		UserInfo info=new UserInfo();
		info.setFirstName("A");
		info.setLastName("D");
		info.setUserName("test");
		info.setPassword("12356");
		info.setComponyName("abc");
		info.setDob("19/10/94");
		info.setEmail("123");
		info.setContactNumber("0000000000");
		info.setUserType("Employee");
		info.setManegerId("123");
		addUser(info);
		return "hello";
	}
	
	
	@GetMapping("/login")
	public LoginResponce setLogin(@RequestParam String username,@RequestParam String password) {
		List<UserInfo> detals=userInfo.getAllUserInfo();	
		LoginResponce loginResponce=new LoginResponce();
		for(int i=0;i<detals.size();i++) {
			if(username.equals(detals.get(i).getUserName())) {
				if(password.equals(detals.get(i).getPassword())) {
					loginResponce.setIsSucess(true);
					loginResponce.setMsg("Login Sucessfully");
					loginResponce.setUserId(detals.get(i).getUserId());
					loginResponce.setUserType(detals.get(i).getUserType());
					/*
					 * if(!fcmId.isEmpty()) { TeamDetal detal12=new TeamDetal();
					 * detal12=detals.get(i); detal12.setFcmId(fcmId); setPlayer(detal12); }
					 */
					break;
				}else {
					loginResponce.setIsSucess(false);
					loginResponce.setMsg("Wrong Password");
					loginResponce.setUserId(00);
					break;
		}
				
			}else if(i==detals.size()-1) {
				loginResponce.setIsSucess(false);
				loginResponce.setMsg("User Not Exist");
				loginResponce.setUserId(00);
				
			}
		}
		return loginResponce;
		
		
	}
	
	
	
	@GetMapping("/getalluser")
	public List<UserInfo> allPlayerData() {
	List<UserInfo> detals=userInfo.getAllUserInfo();	
	return detals;
	}
	
	
	@PostMapping("/addUser")
	public String addUser(@RequestBody UserInfo userInfoPojo) {
		String s=userInfo.insertUserInfo(userInfoPojo);
		return s;
	}
	
	@GetMapping("/deleteuser")
	public String deleteUser(@RequestParam String id) {
	String detals=userInfo.deleteUserInfo(id);	
	return detals;
	}

}
