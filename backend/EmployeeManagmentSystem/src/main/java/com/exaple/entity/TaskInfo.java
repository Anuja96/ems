package com.exaple.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="task_info")
public class TaskInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="task_id")
	int taskId;
	@Column(name="task_name")
	String taskName;
	@Column(name="start_date")
	String startDate;
	@Column(name="end_date")
	String endDate;
	@Column(name="task_desc")
	String taskDescripton;
	@Column(name="task_status")
	String taskStatus;
	@Column(name="user_id")
	int userId;
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getTaskDescripton() {
		return taskDescripton;
	}
	public void setTaskDescripton(String taskDescripton) {
		this.taskDescripton = taskDescripton;
	}
	public String getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	
	
	
	

}
